'''
Login form V0.1 Demo  :: (Python script)
Author: Ahmad Atallah
All rights reserved
Created: 28/8/2017
last Edit :

'''
from flask import Flask, render_template, redirect, request, url_for, session, flash, json, g, send_from_directory
from werkzeug import secure_filename
from flask_mail import Mail,Message
from functools import wraps
import sqlite3
import logging
import os

app = Flask(__name__, template_folder='templates')
app.secret_key = " this is me"
app.error = None
app.username = ''
global doctors

# --------- Configuration for database  -----# 
app.database = "sample.db"
# --------- Configuration for database  -----# 


# ----------- Configuration for sending Google mails (Web service API)-----# 
app.config['MAIL_SERVER']='smtp.gmail.com'
app.config['MAIL_PORT'] = 465
#--- Put here your gmail user and password -------#

app.config['MAIL_USERNAME'] = '*****@gmail.com'
app.config['MAIL_PASSWORD'] = '*********'
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True
mail = Mail(app)
# ----------- Configuration for sending Google mails (Web service API)-----# 


# ----------- Configuration for uploading files -----#

# This is the path to the upload directory
app.config['UPLOAD_FOLDER'] = 'uploads/'
# These are the extension that we are accepting to be uploaded
app.config['ALLOWED_EXTENSIONS'] = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

# For a given file, return whether it's an allowed type or not
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']
          
# ----------- Configuration for uploading files -----#
       
       
def login_required(f):
     @wraps(f)
     def wrap(*args,**kwargs):
          if 'logged_in' in session:
               return f(*args,**kwargs)
          else:
               flash('You need to login first')
               return redirect(url_for ('login'))
     return wrap


@app.route("/" , methods = ['GET','POST'])
def main():

     if request.method == 'POST':
          if request.form['submit'] == 'home':
               return render_template('index.html')
          elif request.form['submit'] == 'request':
               return redirect(url_for('request_process'))
          else:
               return redirect(url_for('login'))
     
     return render_template('index.html')

     
@app.route("/verify", methods = ['GET', 'POST','COPY'] )

def login():
    error = None
    if request.method == 'POST':
     
          if request.form['submit'] == 'Forget':
               return redirect(url_for('forget'))
          
          elif request.form['submit'] == 'home':
               return redirect(url_for('main'))
          
          elif request.form['submit'] == 'request':
               return redirect(url_for('request_process'))
          elif request.form['submit'] == 'datacenter':
               return redirect(url_for('login'))
          else:
               try:
                    _name = request.form['username']
                    _id = request.form['id']
                    if _name and _id :
                          g.db = connect_db()
                          cursor = g.db.execute('select * from doctors')
                          for row in cursor.fetchall():
                          # searching into doctors table to apply credential
                                if  _name != str(row[1]) or  _id != str(row[0]) :
                                        app.error = ' ACCESS DENIED!'
                         
                                else:
                                    session['logged_in'] = True
                                    flash ('You were just logged in')
                                    return redirect(url_for('user_page',username = _name))
               
                          g.db.close()
               except Exception as e:
                     return json.dumps({'error':str(e)})
      
          
    return render_template('verify.html',error = app.error)
@login_required
@app.route("/request",methods = ['GET','POST'])
def  request_process():
     if request.method == 'POST':
          if request.form['submit'] == 'home':
               return redirect(url_for('main'))
          elif request.form['submit'] == 'request':
               return redirect(url_for('request_process'))
          else:
               return redirect(url_for('login'))
     return render_template('request.html')


@app.route("/user_page",methods = ['GET','POST'])
@login_required
def user_page():
        
     if request.method == 'POST' :
          if request.form['submit'] == 'home':
               return redirect(url_for('main'))
          elif request.form['submit'] == 'request':
               return redirect(url_for('request_process'))
          elif request.form['submit'] == 'datacenter':
               return redirect(url_for('login'))
          # Route that will process the file upload
          elif request.form['submit'] == 'upload':
                return redirect(url_for('upload_file'))
                #return redirect(url_for('logout'))
          else:
                session.pop('logged_in', None)
                flash('You were just logged out')
                return redirect (url_for ('main'))
            
         
     return render_template('datacenter.html')
     session.pop('logged_in', None) 
    
@login_required 
@app.route('/uploads', methods=['POST'])
def upload_file():
     file = request.files['file']
     # Check if the file is one of the allowed types/extensions
     if file and allowed_file(file.filename):
                # Make the filename safe, remove unsupported chars
               filename = secure_filename(file.filename)
               # Move the file form the temporal folder to
               # the upload folder we setup
               file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
               # Redirect the user to the uploaded_file route, which
               # will basicaly show on the browser the uploaded file
               #return 'file uploaded successfully'
               flash('File uploaded successfully! ')
               return redirect(url_for('uploaded_file', filename=filename))
                
               
# Get the name of the uploaded file
               
# This route is expecting a parameter containing the name
# of a file. Then it will locate that file on the upload
# directory and show it on the browser, so if the user uploads
# an image, that image is going to be show after the upload
@app.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'],filename)

def connect_db():
     return sqlite3.connect(app.database)

@app.route("/forget", methods=['GET','POST'])
def forget():
     message = 'Dear customer, '
     deliveried = None
     if request.method == 'POST':
          if request.form['submit'] == 'home':
               return redirect(url_for('main'))
          elif request.form['submit'] == 'request':
               return redirect(url_for('request_process'))
          elif request.form['submit'] == 'datacenter':
               return redirect(url_for('login'))
          else:
                try:
                     _email = request.form['email']
                     #print _email
                     g.db = connect_db()
                     cursor = g.db.execute("select doctor_id from doctors where first_name = '%s' " %_email  ) 
                     forgottenID = str(cursor.fetchone()[0])
                     message += '\n Your Id is: '
                     message += forgottenID
                     message += '\n Thank you for trusting our services.\n Regards,'
                
                     # To send
					 # write your gmail here 
                     msg = Message('',sender = '****@gmail.com', recipients =[_email])
                     msg.body = message
                     try:
                         mail.send(msg)
                         deliveried = ' deliveried'
                     except Exception as e:
                         return json.dumps({'error':str(e)})
                    
                except Exception as e:
                    return json.dumps({'error':str(e)})
          
     return render_template('forget.html',deliveried = deliveried)


if __name__ == "__main__":
    app.run(debug = True)
    
