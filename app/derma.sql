-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 28, 2017 at 02:41 AM
-- Server version: 10.1.8-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: 'derma'
--

-- --------------------------------------------------------

--
-- Table structure for table 'company'
--

CREATE TABLE 'company' (
  'Company_Name' varchar(50) NOT NULL,
  'Website' varchar(45) DEFAULT NULL,
  'Address' varchar(45) DEFAULT NULL,
  'Phone_no_1' varchar(45) DEFAULT NULL,
  'Phone_no_2' varchar(45) DEFAULT NULL,
  'Email_1' varchar(45) DEFAULT NULL,
  'Email_2' varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table 'department'
--

CREATE TABLE 'department' (
  'Dep_No' int(11) NOT NULL,
  'Dep_Name' varchar(45) NOT NULL,
  'Capacity' int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table 'devices'
--

CREATE TABLE 'devices' (
  'Code' int(11) NOT NULL,
  'Serial_no' varchar(45) NOT NULL,
  'Model_name' varchar(45) DEFAULT NULL,
  'Company' varchar(45) DEFAULT NULL,
  'Warranty_End' date DEFAULT NULL,
  'Maint_interval' varchar(10) DEFAULT NULL,
  'Last_Maint' date DEFAULT NULL,
  'Next_Maint' date DEFAULT NULL,
  'Uptime' date DEFAULT NULL,
  'Downtime' date DEFAULT NULL,
  'Electric_Class' varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table 'doctors'
--

CREATE TABLE 'doctors' (
  'Doctor_ID' int(11) NOT NULL,
  'First_Name' varchar(45) DEFAULT NULL,
  'Mid_Name' varchar(45) DEFAULT NULL,
  'Last_Name' varchar(45) DEFAULT NULL,
  'Title' varchar(45) DEFAULT NULL,
  'Age' int(11) DEFAULT NULL,
  'Sex' varchar(1) NOT NULL,
  'Clinic_no' int(11) DEFAULT NULL,
  'Access' int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table 'doctors'
--

INSERT INTO 'doctors' ('Doctor_ID', 'First_Name', 'Mid_Name', 'Last_Name', 'Title', 'Age', 'Sex', 'Clinic_no', 'Access') VALUES
(455, 'ahmad', NULL, 'Salem', NULL, 56, 'm', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table 'doc_contacts'
--

CREATE TABLE 'doc_contacts' (
  'Doctor_ID' int(11) NOT NULL,
  'Email_1' varchar(45) NOT NULL,
  'Email_2' varchar(45) DEFAULT NULL,
  'Phone_No_1' varchar(20) NOT NULL,
  'Phone_No_2' varchar(20) DEFAULT NULL,
  'Phone_No_3' varchar(20) DEFAULT NULL,
  'Address' varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table 'medications'
--

CREATE TABLE 'medications' (
  'Patient_no' int(11) NOT NULL,
  'Drug_name' varchar(45) NOT NULL,
  'Dose' int(11) DEFAULT NULL,
  'Quantity' int(11) NOT NULL,
  'Times_day' int(11) NOT NULL,
  'Durarion' varchar(45) DEFAULT NULL,
  'Notes' varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table 'nurse'
--

CREATE TABLE 'nurse' (
  'Nurse ID' int(11) NOT NULL,
  'First_Name' varchar(15) DEFAULT NULL,
  'Mid_Name' varchar(15) DEFAULT NULL,
  'Last_Name' varchar(15) DEFAULT NULL,
  'Age' int(11) DEFAULT NULL,
  'Address' varchar(45) DEFAULT NULL,
  'Email' varchar(45) DEFAULT NULL,
  'Phone_No' varchar(45) DEFAULT NULL,
  'Doctor_ID' int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table 'patient'
--

CREATE TABLE 'patient' (
  'Reg_no' int(11) NOT NULL,
  'First_Visit' date NOT NULL,
  'First_Name' varchar(45) NOT NULL,
  'MI' varchar(45) DEFAULT NULL,
  'Last_Name' varchar(45) NOT NULL,
  'Age' int(11) NOT NULL,
  'Sex' varchar(1) NOT NULL,
  'Phone' varchar(16) NOT NULL,
  'Email' varchar(45) DEFAULT NULL,
  'Address' varchar(45) DEFAULT NULL,
  'Case_Type' varchar(45) DEFAULT NULL,
  'Next_Appoint' date DEFAULT NULL,
  'Doctors_ID' int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table 'company'
--
ALTER TABLE 'company'
  ADD PRIMARY KEY ('Company_Name');

--
-- Indexes for table 'department'
--
ALTER TABLE 'department'
  ADD PRIMARY KEY ('Dep_No','Dep_Name');

--
-- Indexes for table 'devices'
--
ALTER TABLE 'devices'
  ADD PRIMARY KEY ('Code'),
  ADD UNIQUE KEY 'Code_UNIQUE' ('Code'),
  ADD UNIQUE KEY 'Serial no._UNIQUE' ('Serial_no'),
  ADD KEY 'company_idx' ('Company');

--
-- Indexes for table 'doctors'
--
ALTER TABLE 'doctors'
  ADD PRIMARY KEY ('Doctor_ID'),
  ADD UNIQUE KEY 'Doctor ID_UNIQUE' ('Doctor_ID'),
  ADD KEY 'Clinic_idx' ('Clinic_no');

--
-- Indexes for table 'doc_contacts'
--
ALTER TABLE 'doc_contacts'
  ADD UNIQUE KEY 'E-mail 1_UNIQUE' ('Email_1'),
  ADD UNIQUE KEY 'Phone No. 1_UNIQUE' ('Phone_No_1'),
  ADD UNIQUE KEY 'Doctor ID_UNIQUE' ('Doctor_ID'),
  ADD UNIQUE KEY 'E-mail 2_UNIQUE' ('Email_2'),
  ADD UNIQUE KEY 'Phone No. 2_UNIQUE' ('Phone_No_2'),
  ADD UNIQUE KEY 'Phone No. 3_UNIQUE' ('Phone_No_3');

--
-- Indexes for table 'medications'
--
ALTER TABLE 'medications'
  ADD UNIQUE KEY 'Patient ID_UNIQUE' ('Patient_no'),
  ADD UNIQUE KEY 'Drug_UNIQUE' ('Drug_name');

--
-- Indexes for table 'nurse'
--
ALTER TABLE 'nurse'
  ADD PRIMARY KEY ('Nurse ID'),
  ADD UNIQUE KEY 'Phone No._UNIQUE' ('Phone_No'),
  ADD UNIQUE KEY 'E-mail_UNIQUE' ('Email'),
  ADD KEY 'Doctor_idx' ('Doctor_ID');

--
-- Indexes for table 'patient'
--
ALTER TABLE 'patient'
  ADD PRIMARY KEY ('Reg_no'),
  ADD UNIQUE KEY 'Reg. no_UNIQUE' ('Reg_no'),
  ADD UNIQUE KEY 'Phone_UNIQUE' ('Phone'),
  ADD UNIQUE KEY 'E-mail_UNIQUE' ('Email'),
  ADD KEY 'fk_Patient_Doctors1_idx' ('Doctors_ID');

--
-- Constraints for dumped tables
--

--
-- Constraints for table 'devices'
--
ALTER TABLE 'devices'
  ADD CONSTRAINT 'company' FOREIGN KEY ('Company') REFERENCES 'company' ('Company_Name') ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table 'doctors'
--
ALTER TABLE 'doctors'
  ADD CONSTRAINT 'Clinic' FOREIGN KEY ('Clinic_no') REFERENCES 'department' ('Dep_No') ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table 'doc_contacts'
--
ALTER TABLE 'doc_contacts'
  ADD CONSTRAINT 'Doctor ID' FOREIGN KEY ('Doctor_ID') REFERENCES 'doctors' ('Doctor_ID') ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table 'medications'
--
ALTER TABLE 'medications'
  ADD CONSTRAINT 'Patient ID' FOREIGN KEY ('Patient_no') REFERENCES 'patient' ('Reg_no') ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table 'nurse'
--
ALTER TABLE 'nurse'
  ADD CONSTRAINT 'Doctor' FOREIGN KEY ('Doctor_ID') REFERENCES 'doctors' ('Doctor_ID') ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table 'patient'
--
ALTER TABLE 'patient'
  ADD CONSTRAINT 'fk_Patient_Doctors1' FOREIGN KEY ('Doctors_ID') REFERENCES 'doctors' ('Doctor_ID') ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
