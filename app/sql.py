import sqlite3
from sqlite3 import OperationalError

def executeScriptsFromFile(filename,db):
    # Open and read the file as a single buffer
    fd = open(filename, 'r')
    sqlFile = fd.read()
    print(sqlFile)
    fd.close()

    # all SQL commands (split on ';')
    sqlCommands = sqlFile.split(';')

    # Execute every command from the input file
    for command in sqlCommands:
        # This will skip and report errors
        # For example, if the tables do not yet exist, this will skip over
        # the DROP TABLE commands
        try:
            db.execute(command)
        except OperationalError, msg:
            print "Command skipped: ", msg

with sqlite3.connect('sample.db') as connection:
    # get a cursor object used to execute SQL commands
    db = connection.cursor()
    executeScriptsFromFile('derma.sql',db)
    # add some data 
    db.execute('INSERT INTO doctors ("Doctor_ID", "First_Name", "Mid_Name", "Last_Name", "Age", "Sex", "Access") VALUES (4568754,"Ahmad","Yahia","Zakaria", 45,"m",1)')
    db.execute('INSERT INTO doctors ("Doctor_ID", "First_Name", "Mid_Name", "Last_Name", "Age", "Sex", "Access") VALUES (48842,"Esraa","Tarek","Mohammed", 45,"m",1)')